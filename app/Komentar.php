<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    public function komentarLike()
    {
        return $this->hasMany('App\KomentarLike');
    }

    public function LikedBy(User $user)
    {
        return $this->komentarLike->contains('user_id', $user->id);
    }
}
