<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //
        //dd($id);
        $user = User::find(Auth::user()->id);
        $followed = User::find($id);
        $user->following()->attach($id);
        return redirect()->route('profile.show', ['profile' => $followed->profile->id]);
        // $user->following()->detach($id);

        // $request->validate([
        //     'gambar' => 'required|mimes:png,jpg,jpeg|max:5000',
        //     'konten' => 'required',
        // ]);

        // $gambar = $request->gambar;
        // $new_gambar = time() . '-' . $gambar->getClientOriginalName();
        // $post = Post::create([
        //     "gambar" => $new_gambar,
        //     "konten" => $request["konten"],
        //     "user_id" => Auth::user()->id,
        // ]);

        // $gambar->move('gambar/', $new_gambar);

        // return redirect('/post')->with('success', 'Post berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find(Auth::user()->id);
        $followed = User::find($id);
        $user->following()->detach($id);
        return redirect()->route('profile.show', ['profile' => $followed->profile->id]);
        // $post = Post::findorfail($id);
        // $post->delete();

        // $path = "gambar/";
        // File::delete($path . $post->poster);

        // return redirect('/post')->with('success', 'Film berhasil dihapus!');
    }
}
