<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = Auth::user();
        $userIds = $user->following->pluck('id')->toArray();
        $userIds[] = $user->id;
        $posts = Post::whereIn('user_id', $userIds)->orderBy('created_at', 'DESC')->get();
        // $posts = Post::all()->sortByDesc("created_at");
        return view('post.index', compact('posts'));
    }

    public function explore()
    {
        //
        if (Auth::user()) {
            $user = Auth::user();
            $userIds = $user->following->pluck('id')->toArray();
            $userIds[] = $user->id;
            $posts = Post::whereNotIn('user_id', $userIds)->orderBy('created_at', 'DESC')->get();
            // $posts = Post::all()->sortByDesc("created_at");
            return view('post.index', compact('posts'));
        } else {
            $posts = Post::all()->sortByDesc("created_at");
            return view('post.index', compact('posts'));
        }
    }

    public function guest()
    {
        //
        $posts = Post::all();
        // $posts = Post::all()->sortByDesc("created_at");
        return view('post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'gambar' => 'required|mimes:png,jpg,jpeg|max:5000',
            'konten' => 'required',
        ]);

        $gambar = $request->gambar;
        $new_gambar = time() . '-' . $gambar->getClientOriginalName();
        $post = Post::create([
            "gambar" => $new_gambar,
            "konten" => $request["konten"],
            "user_id" => Auth::user()->id,
        ]);

        $gambar->move('gambar/', $new_gambar);

        return redirect('/post')->with('success', 'Post berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::find($id);
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'gambar' => 'mimes:png,jpg,jpeg|max:5000',
            'konten' => 'required',
        ]);

        $post = Post::findorfail($id);

        if ($request->has('gambar')) {
            $path = "gambar/";
            File::delete($path . $post->gambar);
            $gambar = $request->gambar;
            $new_poster = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('gambar/', $new_poster);
            $post_data = [
                "gambar" => $new_poster,
                "konten" => $request["konten"],
            ];
        } else {
            $post_data = [
                "konten" => $request["konten"],
            ];
        }

        $post->update($post_data);

        return redirect('/post')->with('success', 'Post berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::findorfail($id);
        $post->postLike()->delete();
        foreach ($post->komentar as $komentar) {
            $komentar->komentarLike()->delete();
            $komentar->delete();
        }
        $path = "gambar/";
        File::delete($path . $post->gambar);

        $post->delete();


        return redirect('/post')->with('success', 'Post berhasil dihapus!');
    }
}
