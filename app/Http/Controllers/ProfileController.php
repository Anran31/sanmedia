<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $profile = Profile::find($id);
        //dd(Auth::user()->id);
        // dd($profile->user->id);
        $posts = Post::where('user_id', $profile->user->id)->orderBy('created_at', 'DESC')->get();
        // $followed = $profile->user->with('following')
        //     ->whereHas('following', function ($query) {
        //         $query->where('followed_by_id', Auth::user()->id);
        //     })
        //     ->get();
        // ->where('followed_by_id', Auth::user()->id)
        // ->where('follow_id', $profile->user->id)
        // ->first();
        // dd($followed);
        $following = User::find(Auth::user()->id)->isFollowing($profile->user->id);
        // $follower_count = User::find(Auth::user()->id)->followerCount($profile->user->id);
        $follower_count = User::find($profile->user_id)->followed()->count();
        // dd(User::find($profile->user_id)->followed()->count());
        // $following_count = User::find(Auth::user()->id)->followingCount($profile->user->id);
        $following_count = User::find($profile->user_id)->following()->count();
        // dd($following);
        return view('profile.show', compact('profile', 'posts', 'following', 'follower_count', 'following_count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $profile = Profile::find($id);
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'username' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        $profile_data = [
            'username' => $request->username,
            'umur' => $request->umur,
            'bio' => $request->bio,
        ];

        $profile = Profile::find($id)->update($profile_data);
        return redirect()->route('profile.show', ['profile' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
