<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarLike extends Model
{
    //
    protected $table = 'like_komentar';
    protected $guarded = [];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function komentar()
    {
        return $this->belongsTo('App\Komentar');
    }
}
