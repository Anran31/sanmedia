<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $fillable = ['gambar', 'konten', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postLike()
    {
        return $this->hasMany('App\PostLike');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }


    public function likedBy(User $user)
    {
        return $this->postLike->contains('user_id', $user->id);
    }
}
