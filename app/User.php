<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function post()
    {
        return $this->hasOne('App\Post');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function followed()
    {
        return $this->belongsToMany('App\User', 'follow_user', 'follow_id', 'followed_by_id');
    }

    public function following()
    {
        return $this->belongsToMany('App\User', 'follow_user', 'followed_by_id', 'follow_id');
    }

    public function isFollowing(int $id)
    {
        return !!$this->following()->where('follow_id', $id)->count();
    }

    public function followerCount(int $id)
    {
        return $this->following()->where('follow_id', $id)->count();
    }

    public function followingCount(int $id)
    {
        return $this->following()->where('followed_by_id', $id)->count();
    }
    // public function isFollowedBy(User $user)
    // {
    //     return !!$this->followed()->where('user_id', $user->id)->count();
    // }
    public function postLike()
    {
        return $this->hasMany('App\PostLike');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    public function komentarLike()
    {
        return $this->hasMany('App\KomentarLike');
    }
}
