# Final Project

# Kelompok 21

# Anggota Kelompok

- Andika Nugrahanto
- Satrio Hanif Wicaksono
- Dyandra Paramitha Widyadhana

# Tema Project

Social Media

# ERD

![](public/ERD_SanMedia.png)

# Link Video

Link Demo  : https://www.youtube.com/watch?v=upalw2-lHPY

Link Deploy : https://morning-bastion-88690.herokuapp.com/
