<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@explore');

Route::get('/post/explore', 'PostController@explore')->name('post.explore');
Route::group(['middleware' => ['auth']], function () {
    Route::resource('post', 'PostController', ['except' => ['show']]);

    Route::resource('profile', 'ProfileController');

    Route::post('/follow/{follow}', 'FollowController@store')->name('follow.store');
    Route::delete('/follow/{follow}', 'FollowController@destroy')->name('follow.destroy');

    Route::post('/postlike/{postlike}', 'PostLikeController@store')->name('postlike.store');
    Route::delete('/postlike/{postlike}', 'PostLikeController@destroy')->name('postlike.destroy');

    Route::resource('komentar', 'KomentarController');

    Route::post('/komentarlike/{komentarlike}', 'KomentarLikeController@store')->name('komentarlike.store');
    Route::delete('/komentarlike/{komentarlike}', 'KomentarLikeController@destroy')->name('komentarlike.destroy');
});

Route::resource('post', 'PostController', ['only' => ['show']]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
