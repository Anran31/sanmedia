<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="../../index3.html" class="brand-link">
        <img src="{{ asset('/adminlte/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">SanMedia</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('/adminlte/dist/img/guest.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            @auth
            <div class="info">
                <a href="{{ route('profile.show',['profile' => Auth::user()->profile->id]) }}"
                    class="d-block">{{ Auth::user()->profile->username }}</a>
            </div>
            @endauth
            @guest
            <div class="info">
                <a href="#" class="d-block">Guest</a>
            </div>
            @endguest
        </div>

        <!-- SidebarSearch Form -->
        {{-- <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div> --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('post.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Home
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('post.explore') }}" class="nav-link">
                        <i class="nav-icon fas fa-search"></i>
                        <p>
                            Explore
                        </p>
                    </a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link bg-primary" href="{{ route('login') }}">
                        <i class="nav-icon fas fa-sign-in-alt"></i>
                        <p>{{ __('Login') }}</p>
                    </a>
                </li>
                @endguest
                @auth
                <li class="nav-item">
                    <a href="{{ route('profile.show',['profile' => Auth::user()->profile->id]) }}" class="nav-link">
                        <i class="nav-icon far fa-user"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link bg-danger" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>{{ __('Logout') }}</p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
                @endauth
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>