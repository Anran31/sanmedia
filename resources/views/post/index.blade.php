@extends('adminlte.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                @if(Route::is('post.index'))
                <h1>Home</h1>
                @else
                <h1>Explore</h1>
                @endif
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.col -->
            <div class="col-md-12">
                <div class="card">
                    @auth
                    <div class="card-header p-2">
                        @if(Route::is('post.index'))
                        <a href="{{ route('post.create') }}" class="btn btn-primary my-2">Add Post</a>
                        @endif
                    </div><!-- /.card-header -->
                    @endauth
                    <div class="card-body">
                        @forelse ($posts as $post)
                        <!-- Post -->
                        <div class="post">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm" src="{{ asset('adminlte/dist/img/guest.png') }}"
                                    alt="user image">
                                <span class="username">
                                    <a
                                        href="{{ route('profile.show',['profile' => $post->user->profile->id]) }}">{{ $post->user->profile->username }}</a>

                                </span>
                                <span class="description">{{ $post->created_at->format('H:i d/m/Y') }}</span>
                            </div>
                            <!-- /.user-block -->

                            <div class="row mb-3">
                                <div class="col-sm-6">
                                    <img class="img-fluid" src="{{ asset('gambar/' . $post->gambar) }}" alt="Photo">
                                </div>
                                <div class="row ml-2">
                                    <p>
                                        {{ $post->konten }}
                                    </p>
                                </div>
                            </div>

                            <p>
                                <a href="#" class="link-black text-sm btn-link disabled"><i
                                        class="far fa-thumbs-up mr-1"></i>
                                    {{ $post->postLike->count() }} Like</a>
                                <a href="{{ route('post.show',['post' => $post->id]) }}"
                                    class="link-black text-sm btn-link disabled">
                                    <i class="far fa-comments mr-1"></i> Comments
                                    ({{ $post->komentar->count() }})
                                </a>
                                @auth
                                <div class="d-flex">
                                    @if (!$post->likedBy(auth()->user()))
                                    <form action="{{ route('postlike.store',['postlike'=>$post->id]) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-primary btn-sm  mr-2"><i
                                                class="far fa-heart ml-0 mr-1"></i> Like</button>
                                    </form>
                                    @else
                                    <form action="{{ route('postlike.destroy',['postlike'=>$post->id]) }}"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-secondary btn-sm  mr-2"><i
                                                class="fas fa-heart-broken ml-0 mr-1"></i> Unlike</button>
                                    </form>
                                    @endif
                                    <a href="{{ route('post.show',['post' => $post->id]) }}"
                                        class="btn btn-primary btn-sm  mr-2"><i class="far fa-comments ml-1 mr-1"></i>
                                        Comment</a>
                                    @if ($post->user_id === Auth::user()->id)
                                    <a href="{{ route('post.edit',['post' => $post->id]) }}"
                                        class="btn btn-info btn-sm  mr-2"><i class="far fa-edit ml-1 mr-1"></i>
                                        Edit</a>
                                    <form action="{{ route('post.destroy', ["post" => $post->id]) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger btn-sm  ml-0"><i
                                                class="far fa-trash-alt ml-1 mr-1"></i> Delete</button>
                                    </form>
                                    @endif
                                </div>
                                @endauth
                                @guest
                                <div class="d-flex">
                                    <a href="{{ route('post.show',['post' => $post->id]) }}"
                                        class="btn btn-primary btn-sm  mr-2"><i class="far fa-comments ml-1 mr-1"></i>
                                        Comment</a>
                                </div>
                                @endguest
                            </p>
                        </div>
                        <!-- /.post -->
                        @empty
                        @if(Route::is('post.index'))
                        <p>No Post Yet</p>
                        @else
                        <p>No Post to Explore Yet</p>
                        @endif

                        @endforelse
                        <div class="tab-content">
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection