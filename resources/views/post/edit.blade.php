@extends('adminlte.master')

@section('content')
<div class="card card-primary mt-3 mx-3">
    <div class="card-header">
        <h3 class="card-title">Edit Post</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ route('post.update',['post'=>$post->id])}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="gambar">Gambar</label>
                <input type="file" class="form-control" id="gambar" name="gambar"
                    value="{{ old('gambar',$post->gambar) }}" placeholder="Masukkan gambar">
                @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="konten">Konten</label>
                <textarea name="konten" id="konten" class="form-control"
                    placeholder="konten">{{ old('konten',$post->konten) }}</textarea>
                @error('konten')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>
@endsection