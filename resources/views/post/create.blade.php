@extends('adminlte.master')

@section('content')
<div class="card card-primary mt-3 mx-3">
    <div class="card-header">
        <h3 class="card-title">Add Post</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ route('post.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="gambar">Gambar</label>
                <input type="file" class="form-control" id="gambar" name="gambar" value="{{ old('gambar','') }}"
                    placeholder="Masukkan gambar">
                @error('gambar')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="konten">Konten</label>
                <textarea name="konten" id="konten" class="form-control"
                    placeholder="konten">{{ old('konten','') }}</textarea>
                @error('konten')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Add</button>
        </div>
    </form>
</div>
@endsection