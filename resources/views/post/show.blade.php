@extends('adminlte.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Post</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.col -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <!-- Post -->
                        <div class="post">
                            <div class="user-block">
                                <img class="img-circle img-bordered-sm" src="{{ asset('adminlte/dist/img/guest.png') }}"
                                    alt="user image">
                                <span class="username">
                                    <a href="#">{{ $post->user->profile->username }}</a>
                                </span>
                                <span class="description">{{ $post->created_at->format('H:i d/m/Y') }}</span>
                            </div>
                            <!-- /.user-block -->
                            <div class="col mb-3">
                                <div class="row-sm-6">
                                    <img class="img-fluid" src="{{ asset('gambar/' . $post->gambar) }}" alt="Photo">
                                </div>
                                <div class="row">
                                    <p>
                                        {{ $post->konten }}
                                    </p>
                                </div>
                            </div>
                            <p>
                                <a href="#" class="link-black text-sm btn-link disabled"><i
                                        class="far fa-thumbs-up mr-1"></i>
                                    {{ $post->postLike->count() }} Like</a>
                                <a href="#" class="link-black text-sm btn-link disabled">
                                    <i class="far fa-comments mr-1"></i> Comments
                                    ({{ $post->komentar->count() }})
                                </a>
                                @auth
                                <div class="d-flex">
                                    @if (!$post->likedBy(auth()->user()))
                                    <form action="{{ route('postlike.store',['postlike'=>$post->id]) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-primary btn-sm  mr-2"><i
                                                class="far fa-heart ml-0 mr-1"></i> Like</button>
                                    </form>
                                    @else
                                    <form action="{{ route('postlike.destroy',['postlike'=>$post->id]) }}"
                                        method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-secondary btn-sm  mr-2"><i
                                                class="fas fa-heart-broken ml-0 mr-1"></i> Unlike</button>
                                    </form>
                                    @endif
                                    @if ($post->user_id === Auth::user()->id)
                                    <a href="{{ route('post.edit',['post' => $post->id]) }}"
                                        class="btn btn-info btn-sm  mr-2"><i class="far fa-edit ml-1 mr-1"></i>
                                        Edit</a>
                                    <form action="{{ route('post.destroy', ["post" => $post->id]) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="btn btn-danger btn-sm  ml-0"><i
                                                class="far fa-trash-alt ml-1 mr-1"></i> Delete</button>
                                    </form>
                                    @endif
                                </div>
                                @endauth
                                {{-- <span class="float-right">
                                    <a href="{{ route('post.show',['post' => $post->id]) }}" class="link-black
                                text-sm">
                                <i class="far fa-comments mr-1"></i> Comments (5)
                                </a>
                                </span> --}}
                            </p>
                            <!-- /.row -->
                            @auth
                            <div>
                                <form method="POST" action="{{ route('komentar.store') }}">
                                    @csrf
                                    <div class="input-group mb-3">
                                        <textarea name="komen" id="komen"
                                            class="form-control @error('komen') is-invalid @enderror"
                                            placeholder="Enter Comment">{{ old('komen','') }}</textarea>
                                        @error('komen')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <input type="hidden" name="post_id" value="{{ $post->id }}">
                                    <button type="submit" class="btn btn-primary btn-block">Add Comment</button>
                                </form>
                            </div>
                            <hr>
                        </div>
                        @endauth
                        <!-- /.post -->

                    </div><!-- /.card-body -->
                    <div class="card-footer card-comments">
                        @forelse ($post->komentar as $komen)
                        <div class="card-comment">
                            <!-- User image -->
                            <img class="img-circle img-sm" src="{{ asset('adminlte/dist/img/guest.png') }}"
                                alt="User Image">

                            <div class="comment-text">
                                <span class="username">
                                    {{ $komen->user->profile->username }}
                                    <span
                                        class="text-muted float-right">{{ $komen->created_at->format('H:i d/m/Y') }}</span>
                                </span><!-- /.username -->
                                <div class="col">
                                    <div class="row">
                                        {{ $komen->komen }}
                                    </div>
                                    <div class="row">
                                        <a href="#" class="link-black text-sm btn-link disabled"><i
                                                class="far fa-thumbs-up mr-1"></i>
                                            {{ $komen->komentarLike->count() }} Like</a>
                                    </div>
                                    <div class="row">
                                        <p>
                                            @auth
                                            <div class="d-flex">
                                                @if (!$komen->likedBy(auth()->user()))
                                                <form
                                                    action="{{ route('komentarlike.store',['komentarlike'=>$komen->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary btn-sm  mr-2"><i
                                                            class="far fa-heart ml-0 mr-1"></i> Like</button>
                                                </form>
                                                @else
                                                <form
                                                    action="{{ route('komentarlike.destroy',['komentarlike'=>$komen->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-secondary btn-sm  mr-2"><i
                                                            class="fas fa-heart-broken ml-0 mr-1"></i> Unlike</button>
                                                </form>
                                                @endif
                                                @if ($komen->user_id === Auth::user()->id)
                                                <form
                                                    action="{{ route('komentar.destroy', ["komentar" => $komen->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method("DELETE")
                                                    <button type="submit" class="btn btn-danger btn-sm  ml-0"><i
                                                            class="far fa-trash-alt ml-1 mr-1"></i> Delete</button>
                                                </form>
                                                @endif
                                            </div>
                                            @endauth
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.comment-text -->
                        </div>
                        @empty
                        <p>No Comment</p>
                        @endforelse
                    </div>
                    <!-- /.card-footer -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
