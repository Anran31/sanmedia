@extends('adminlte.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                <h1>Profile</h1>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        {{-- <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg"
                                alt="User profile picture">
                        </div> --}}

                        <h3 class="profile-username text-center">{{ $profile->user->name }}</h3>

                        <p class="text-muted text-center">{{ $profile->username }}</p>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Followers</b> <a class="float-right">{{ $follower_count }}</a>
                            </li>
                            <li class="list-group-item">
                                <b>Following</b> <a class="float-right">{{ $following_count }}</a>
                            </li>
                        </ul>
                        @if ($profile->user_id !== Auth::user()->id and !$following)
                        <form id="follow" action="{{ route('follow.store',['follow'=>$profile->user_id]) }}"
                            method="POST">
                            @csrf
                            {{-- <a href="#" onclick="submit()" class="btn btn-primary btn-block"><b>Follow</b></a> --}}
                            <button type="submit" class="btn btn-primary btn-block"><b>Follow</b></button>
                        </form>
                        @elseif($following)
                        <form id="follow" action="{{ route('follow.destroy',['follow'=>$profile->user_id]) }}"
                            method="POST">
                            @csrf
                            @method('DELETE')
                            {{-- <a href="#" onclick="submit()" class="btn btn-danger btn-block"><b>Unfollow</b></a> --}}
                            <button type="submit" class="btn btn-danger btn-block"><b>Unfollow</b></button>
                        </form>
                        @else
                        <a href="{{ route('profile.edit',['profile'=>Auth::user()->profile->id]) }}"
                            class="btn btn-primary btn-block"><b>Edit Profile</b></a>
                        @endif
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fas fa-birthday-cake mr-1"></i> Umur</strong>

                        <p class="text-muted">
                            {{ $profile->umur }} tahun
                        </p>

                        <hr>

                        <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

                        <p class="text-muted">{{ $profile->bio }}</p>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="card">
                    @auth
                    <div class="card-header p-2">
                        @if($profile->user->id === Auth::user()->id)
                        <a href="{{ route('post.create') }}" class="btn btn-primary my-2">Add Post</a>
                        @endif
                    </div><!-- /.card-header -->
                    @endauth
                    {{-- <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link active" href="#posts" data-toggle="tab">Posts</a>
                            </li>
                        </ul>
                    </div><!-- /.card-header --> --}}
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="posts">
                                @forelse ($posts as $post)
                                <!-- Post -->
                                <div class="post">
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                            src="{{ asset('adminlte/dist/img/guest.png') }}" alt="user image">
                                        <span class="username">
                                            <a
                                                href="{{ route('profile.show',['profile' => $post->user->profile->id]) }}">{{ $post->user->profile->username }}</a>

                                        </span>
                                        <span class="description">{{ $post->created_at->format('H:i d/m/Y') }}</span>
                                    </div>
                                    <!-- /.user-block -->

                                    <div class="row mb-3">
                                        <div class="col-sm-6">
                                            <img class="img-fluid" src="{{ asset('gambar/' . $post->gambar) }}"
                                                alt="Photo">
                                        </div>
                                        <div class="row ml-2">
                                            <p>
                                                {{ $post->konten }}
                                            </p>
                                        </div>
                                    </div>

                                    <p>
                                        <a href="#" class="link-black text-sm btn-link disabled"><i
                                                class="far fa-thumbs-up mr-1"></i>
                                            {{ $post->postLike->count() }} Like</a>
                                        <a href="{{ route('post.show',['post' => $post->id]) }}"
                                            class="link-black text-sm btn-link disabled">
                                            <i class="far fa-comments mr-1"></i> Comments
                                            ({{ $post->komentar->count() }})
                                        </a>
                                        @auth
                                        <div class="d-flex">
                                            @if (!$post->likedBy(auth()->user()))
                                            <form action="{{ route('postlike.store',['postlike'=>$post->id]) }}"
                                                method="POST">
                                                @csrf
                                                <button type="submit" class="btn btn-primary btn-sm  mr-2"><i
                                                        class="far fa-heart ml-0 mr-1"></i> Like</button>
                                            </form>
                                            @else
                                            <form action="{{ route('postlike.destroy',['postlike'=>$post->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-secondary btn-sm  mr-2"><i
                                                        class="fas fa-heart-broken ml-0 mr-1"></i> Unlike</button>
                                            </form>
                                            @endif
                                            <a href="{{ route('post.show',['post' => $post->id]) }}"
                                                class="btn btn-primary btn-sm  mr-2"><i
                                                    class="far fa-comments ml-1 mr-1"></i>
                                                Comment</a>
                                            @if ($post->user_id === Auth::user()->id)
                                            <a href="{{ route('post.edit',['post' => $post->id]) }}"
                                                class="btn btn-info btn-sm  mr-2"><i class="far fa-edit ml-1 mr-1"></i>
                                                Edit</a>
                                            <form action="{{ route('post.destroy', ["post" => $post->id]) }}"
                                                method="POST">
                                                @csrf
                                                @method("DELETE")
                                                <button type="submit" class="btn btn-danger btn-sm  ml-0"><i
                                                        class="far fa-trash-alt ml-1 mr-1"></i> Delete</button>
                                            </form>
                                            @endif
                                        </div>
                                        @endauth
                                    </p>

                                </div>
                                <!-- /.post -->
                                @empty
                                <p>No Post Yet</p>
                                @endforelse
                            </div>
                            <!-- /.tab-pane -->

                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection

@push('scripts')
<script>
    function submit() {
        document.getElementById("follow").submit();
    }
</script>
@endpush
