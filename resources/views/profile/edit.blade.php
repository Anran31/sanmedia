@extends('adminlte.master')

@section('content')
<div class="card card-primary mt-3 mx-3">
    <div class="card-header">
        <h3 class="card-title">Edit Profile</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ route('profile.update',['profile' => $profile->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="username" name="username"
                    value="{{ old('username',$profile->username) }}" placeholder="Masukkan username">
                @error('username')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur',$profile->umur) }}"
                    placeholder="Masukkan umur">
                @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea name="bio" id="bio" class="form-control @error('bio') is-invalid @enderror"
                    placeholder="Bio">{{ old('bio',$profile->bio) }}</textarea>
                @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
        </div>
    </form>
</div>
@endsection